import java.io.*;

//записування файла
public class FileWorker {

    //створення методу для записування файла
    public static void write(String fileName, String text) {

        //визначаємо файл
        File file = new File(fileName);

        try {

            //перевіряємо, якщо файл не існує то створюємо його
            if (!file.exists()) {
                file.createNewFile();
            }

            //PrintWriter забезпечить можливості запису в файл
            PrintWriter printWriter = new PrintWriter(file.getAbsoluteFile());

            try {

                //записуємо текст у файл
                printWriter.print(text);
            } finally {

                // після чого ми повинні закрити файл
                // інакше файл не запишеться
                printWriter.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static String text = "1.country city street houseNumber\n" +
            "2.country city street houseNumber\n" +
            "3.country city street houseNumber\n" +
            "4.country city street houseNumber\n";
    static String fileName = "text1";

    //створення методу для читання файла
    public static String read(String fileName) throws FileNotFoundException {
        //це спец. об'єкт для створеня рядка
        StringBuilder stringBuilder = new StringBuilder();
        exists(fileName);

        try {

            //об'єкт для читання файла в буфер
            File file = new File(fileName);
            BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            try {

                //в циклі по рядково зчитуємо файл
                String s;
                while ((s = in.readLine()) != null) {
                    stringBuilder.append(s);
                    stringBuilder.append("\n");
                }
            } finally {

                //закриваємо файл
                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        //повертаємо отриманий текст з файлу
        return stringBuilder.toString();
    }

    private static void exists(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        if (!file.exists()) {
            throw new FileNotFoundException(file.getName());
        }
    }

    //створення методу update
    public static void update(String nameFile, String newText) throws FileNotFoundException {
        exists(fileName);
        StringBuilder stringBuilder = new StringBuilder();
        String oldFile = read(nameFile);
        stringBuilder.append(oldFile);
        stringBuilder.append(newText);
        write(nameFile, stringBuilder.toString());
    }


    public static void main(String[] args) throws IOException {
        //запис в файл
        FileWorker.write(fileName, text);

        //читання файлу
        String textFromFile = FileWorker.read(fileName);
        System.out.println(textFromFile);

        //оновлення файлу
        FileWorker.update(fileName, "5.country city street houseNumber\n");
        String string = FileWorker.read(fileName);
        System.out.println(string);

        //видалення
        try {
            File file = new File(fileName);
            BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            String s = "";

            // зчитуємо файл
            while ((string = in.readLine()) != null) {
                s += string;
            }
            System.out.println(s.substring(s.indexOf("5"), s.length()));

            //PrintWriter забезпечить можливості запису в файл
            PrintWriter printWriter = new PrintWriter(file.getAbsoluteFile());
            try {

                //записуємо текст у файл
               printWriter.print(s.substring(s.indexOf("5"), s.length()));
            } finally {

                // після чого ми повинні закрити файл
                // інакше файл не запишеться
                printWriter.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}










